// Insert Document 

db.fruits.insertMany(
	[
  			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
	])

// $count operator - total number of fruits on sale

db.fruits.aggregate([
    { 
    	$match: {onSale:true},
    },
    {
    	$count: "fruitsOnSale",
    }
  ])


// $count operator - total number of fruits with stock more than 20

db.fruits.aggregate([
    { 
    	$match: {stock:{$gt:20}},
    },
    {
    	$count: "enoughStock",
    }
  ])


// $average operator - average price of fruits on sale per supplier 

db.fruits.aggregate([
	{
		$match: { onSale: true}
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_price: { $avg: "$price"},
		}
	},])


// $max operator 

db.fruits.aggregate([
	{
		$match: { onSale: true}
	},
	{
		$group: {
			_id: "$supplier_id",
			max_price: { $max: "$price"},
		}
	},])


// $min operator

db.fruits.aggregate([
	{
		$match: { onSale: true}
	},
	{
		$group: {
			_id: "$supplier_id",
			min_price: { $min: "$price"},
		}
	},])
